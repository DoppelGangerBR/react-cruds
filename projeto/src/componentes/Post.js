//https://www.youtube.com/watch?v=7A4UQGrFU9Q

import React from 'react';
import Comentario from './Comentario';
export default class Post extends React.Component{
    constructor(props){
        super(props);
        this.state = {

            comentarios:[
                {text: 'Ótimo post!'}
            ],
            novoComentario:''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
    }
    handleTextChange(e){
        this.setState({novoComentario: e.target.value})
    }

    handleSubmit(e){
        this.setState({
            comentarios:[
                ...this.state.comentarios,
                {text: this.state.novoComentario}
            ]
        })
        this.setState({ novoComentario: ''})
        e.preventDefault();
    }

    render(){
        return(
            <div>
                <h2>{this.props.title}</h2>
                <form onSubmit={this.handleSubmit}>
                    <input value={this.state.novoComentario} onChange={this.handleTextChange}/>
                    <button type="submit">Comentar</button>
                </form>
                { this.state.comentarios.map((comentarios, index) => {
                    return <Comentario key={index} text={comentarios.text} />
                })}
                
            </div>
        );
    }
}