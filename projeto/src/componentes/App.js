import React from 'react';


import Post from './Post'
export default class App extends React.Component {
    render(){
        return (
            <div>
                <h1>Olá Mundo!</h1>
                <Post title="Post Inicial"/>
                <Post title="Post 3"/>
                <Post title="Post 5"/>
                <Post title="Post 2"/>
            </div>
            );
    }
}